import React from "react";
import ItemShoes from "./ItemShoes";
import CardShoes from "./CardShoes";
import { data_shoes } from "./data_shoes";
import { useState } from "react";

export default function Ex_Shoes_Hook() {
  const [shoe, setShoe] = useState({
    listShoe: data_shoes,
    cart: [],
  });
  let renderContent = () => {
    return shoe.listShoe.map((item, index) => {
      return (
        <ItemShoes key={index} handleAddToCart={handleAddToCart} data={item} />
      );
    });
  };

  let handleAddToCart = (shoes) => {
    let index = shoe.cart.findIndex((item) => {
      return item.id == shoes.id;
    });
    let cloneCart = [...shoe.cart];
    if (index == -1) {
      let newShoes = { ...shoes, quantity: 1 };
      cloneCart.push(newShoes);
    } else {
      cloneCart[index].quantity++;
    }
    setShoe({
      ...shoe,
      cart: cloneCart,
    });
  };
  let handleQuantity = (idShoe, value) => {
    let index = shoe.cart.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index == -1) return;
    let cloneCart = [...shoe.cart];
    cloneCart[index].quantity = cloneCart[index].quantity + value;
    cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);
    setShoe({
      ...shoe,
      cart: cloneCart,
    });
  };
  return (
    <div className="container p-5">
      <CardShoes handleQuantity={handleQuantity} cart={shoe.cart} />
      <div className="row">{renderContent()}</div>
    </div>
  );
}
