import React from "react";

export default function ItemShoes(shoe) {
  return (
    <div className="col-3 mt-5">
      <div className="card h-100 " style={{ width: "100%" }}>
        <img
          className="card-img-top"
          src={shoe.data.image}
          alt="Card image cap"
        />
        <div className="card-body">
          <h5 className="card-title">{shoe.data.name}</h5>
          <p className="text-danger ">{shoe.data.price} $</p>
          <button
            onClick={() => {
              shoe.handleAddToCart(shoe.data);
            }}
            className="btn btn-secondary "
          >
            Add to card
          </button>
        </div>
      </div>
    </div>
  );
}
